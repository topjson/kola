package com.base.server.modules.power.service.impl;

import com.base.server.modules.power.entity.MenuInfo;
import com.base.server.modules.power.mapper.MenuInfoMapper;
import com.base.server.modules.power.service.MenuInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class MenuInfoServiceImpl extends ServiceImpl<MenuInfoMapper, MenuInfo> implements MenuInfoService {

}
