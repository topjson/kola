package com.base.server.modules.power.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 菜单权限关联表
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_menu_permission_relation")
@ApiModel(value="MenuPermissionRelation对象", description="菜单权限关联表")
public class MenuPermissionRelation extends Model<MenuPermissionRelation> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "菜单id")
    private String menuId;

    @ApiModelProperty(value = "权限id")
    private String permissionId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
