package com.base.server.modules.power.mapper;

import com.base.server.modules.power.entity.MenuInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface MenuInfoMapper extends BaseMapper<MenuInfo> {

}
