package com.base.server.modules.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.user.entity.UserGroupInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户组表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserGroupInfoService extends IService<UserGroupInfo> {
    IPage<UserGroupInfo> selectByPage(Page<UserGroupInfo> page, UserGroupInfo userGroupInfo);
}
