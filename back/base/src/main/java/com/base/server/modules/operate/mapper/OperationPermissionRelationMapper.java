package com.base.server.modules.operate.mapper;

import com.base.server.modules.operate.entity.OperationPermissionRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限与功能操作关联表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface OperationPermissionRelationMapper extends BaseMapper<OperationPermissionRelation> {

}
