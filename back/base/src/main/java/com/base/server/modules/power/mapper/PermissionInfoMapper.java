package com.base.server.modules.power.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.power.entity.PermissionInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.base.server.modules.power.entity.PermissionInfoVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface PermissionInfoMapper extends BaseMapper<PermissionInfo> {

    /**
     * 分页查询
     *
     * @param page
     * @param query
     * @return
     */
    IPage<PermissionInfoVo> selectByMyPage(Page<PermissionInfoVo> page, @Param(Constants.WRAPPER) QueryWrapper<PermissionInfoVo> query);
}
