package com.base.server.common.config.jwt;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * 描述：JWT 与shiro 集成
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/9 14:53
 */
@Data
public class JwtToken implements AuthenticationToken {

    private static final long serialVersionUID = -1531262845771645068L;

    //加密密钥(包含用户信息)
    private String token;

    public JwtToken(String token) {
        this.token = token;
    }


    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

}
