package com.base.server.modules.operate.mapper;

import com.base.server.modules.operate.entity.OperationInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 功能操作表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface OperationInfoMapper extends BaseMapper<OperationInfo> {

}
