package com.base.server.modules.user.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.user.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.server.modules.user.entity.UserInfoVo;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserInfoService extends IService<UserInfo> {

    boolean save(UserInfoVo userInfo);

    /**
     * 分页查询
     *
     * @param page
     * @param userInfoVo
     * @return
     */
    IPage<UserInfoVo> selectByMyPage(Page<UserInfoVo> page, UserInfoVo userInfoVo);

    boolean updateById(UserInfoVo userInfo);

    List<UserInfoVo> list(UserInfoVo userInfo);

    UserInfoVo selectOne(UserInfoVo userInfoVo);

    /**
     * 修改用户信息
     *
     * @param userInfo
     */
    boolean userInfoUpdateByOne(UserInfoVo userInfo);
}
