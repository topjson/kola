package com.base.server.modules.user.service.impl;

import com.base.server.modules.user.entity.UserGroupRelation;
import com.base.server.modules.user.mapper.UserGroupRelationMapper;
import com.base.server.modules.user.service.UserGroupRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户组与用户关联表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class UserGroupRelationServiceImpl extends ServiceImpl<UserGroupRelationMapper, UserGroupRelation> implements UserGroupRelationService {

}
