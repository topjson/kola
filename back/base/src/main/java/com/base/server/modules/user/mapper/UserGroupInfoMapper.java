package com.base.server.modules.user.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.base.server.modules.user.entity.UserGroupInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户组表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserGroupInfoMapper extends BaseMapper<UserGroupInfo> {

    IPage<UserGroupInfo> selectByPage(Page<UserGroupInfo> page, @Param(Constants.WRAPPER) QueryWrapper<UserGroupInfo> queryWrapper);
}
