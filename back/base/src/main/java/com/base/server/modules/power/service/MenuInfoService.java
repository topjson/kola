package com.base.server.modules.power.service;

import com.base.server.modules.power.entity.MenuInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface MenuInfoService extends IService<MenuInfo> {

}
