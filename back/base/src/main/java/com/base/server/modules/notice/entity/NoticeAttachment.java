package com.base.server.modules.notice.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 通知附件表
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_base_notice_attachment")
@ApiModel(value="NoticeAttachment对象", description="通知附件表")
public class NoticeAttachment extends Model<NoticeAttachment> {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "通知主键id")
    private String noticeId;

    @ApiModelProperty(value = "附件id")
    private String attachmentId;

    @ApiModelProperty(value = "附件名称")
    private String attachmentName;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
