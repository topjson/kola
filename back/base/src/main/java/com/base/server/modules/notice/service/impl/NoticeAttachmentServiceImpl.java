package com.base.server.modules.notice.service.impl;

import com.base.server.modules.notice.entity.NoticeAttachment;
import com.base.server.modules.notice.mapper.NoticeAttachmentMapper;
import com.base.server.modules.notice.service.NoticeAttachmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 通知附件表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2020-01-16
 */
@Service
public class NoticeAttachmentServiceImpl extends ServiceImpl<NoticeAttachmentMapper, NoticeAttachment> implements NoticeAttachmentService {

}
