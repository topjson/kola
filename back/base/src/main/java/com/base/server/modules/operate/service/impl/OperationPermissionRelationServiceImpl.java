package com.base.server.modules.operate.service.impl;

import com.base.server.modules.operate.entity.OperationPermissionRelation;
import com.base.server.modules.operate.mapper.OperationPermissionRelationMapper;
import com.base.server.modules.operate.service.OperationPermissionRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限与功能操作关联表 服务实现类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Service
public class OperationPermissionRelationServiceImpl extends ServiceImpl<OperationPermissionRelationMapper, OperationPermissionRelation> implements OperationPermissionRelationService {

}
