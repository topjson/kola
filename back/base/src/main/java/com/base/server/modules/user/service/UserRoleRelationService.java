package com.base.server.modules.user.service;

import com.base.server.modules.user.entity.UserRoleRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户与角色关联表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserRoleRelationService extends IService<UserRoleRelation> {

}
