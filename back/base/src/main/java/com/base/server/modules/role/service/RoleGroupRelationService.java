package com.base.server.modules.role.service;

import com.base.server.modules.role.entity.RoleGroupRelation;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色与用户组关联表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface RoleGroupRelationService extends IService<RoleGroupRelation> {

}
