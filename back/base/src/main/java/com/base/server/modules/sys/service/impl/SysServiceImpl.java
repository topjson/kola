package com.base.server.modules.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.base.server.modules.sys.service.SysService;
import com.base.server.modules.user.entity.UserInfo;
import com.base.server.modules.user.entity.UserInfoVo;
import com.base.server.modules.user.mapper.UserInfoMapper;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 描述：系统Service实现类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/12/16 15:28 星期一
 */
@Service
public class SysServiceImpl implements SysService {

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public Map<String, Object> login(UserInfo userInfo) {
        QueryWrapper<UserInfo> queryWrapper = new QueryWrapper<>(userInfo);
        queryWrapper.eq("name", userInfo.getName());
        queryWrapper.eq("password", userInfo.getPassword());
        UserInfo user = userInfoMapper.selectOne(queryWrapper);

        Map<String, Object> map = Maps.newHashMap();
        if (user != null) {
            map.put("id", user.getId());
            map.put("name", user.getName());
            map.put("loginName", user.getLoginName());
            map.put("roleName", "总经理");
            map.put("header", user.getHeader());
            map.put("sex", user.getSex());
        }

        return map;
    }

    @Override
    public UserInfoVo loginByName(String account) {
        UserInfoVo userInfo = new UserInfoVo();
        userInfo.setLoginName(account);
        QueryWrapper<UserInfoVo> queryWrapper = new QueryWrapper<>(userInfo);
        queryWrapper.eq("login_name", account);
        return userInfoMapper.selectOne(queryWrapper);
    }
}
