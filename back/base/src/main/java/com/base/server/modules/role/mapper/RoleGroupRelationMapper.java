package com.base.server.modules.role.mapper;

import com.base.server.modules.role.entity.RoleGroupRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色与用户组关联表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface RoleGroupRelationMapper extends BaseMapper<RoleGroupRelation> {

}
