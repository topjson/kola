package com.base.server.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 登录信息
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
@Data
@ApiModel(value = "LoginVo 对象", description = "LoginVo")
public class LoginVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "登录名")
    private String loginName;

    @ApiModelProperty(value = "记住我")
    private boolean rememberMe;
}
