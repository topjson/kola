package com.base.server.modules.user.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.base.server.modules.user.entity.UserInfo;
import com.base.server.modules.user.entity.UserInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    IPage<UserInfoVo> selectMyPage(IPage<UserInfoVo> page, @Param(Constants.WRAPPER) Wrapper<UserInfoVo> queryWrapper);

    List<UserInfoVo> selectList(@Param(Constants.WRAPPER) QueryWrapper<UserInfoVo> queryWrapper);

    UserInfoVo selectOne(@Param(Constants.WRAPPER) QueryWrapper<UserInfoVo> queryWrapper);
}
