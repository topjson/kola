package com.base.server.modules.user.mapper;

import com.base.server.modules.user.entity.UserRoleRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户与角色关联表 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-11-28
 */
public interface UserRoleRelationMapper extends BaseMapper<UserRoleRelation> {

    /**
     * 根据用户id删除
     *
     * @param uid 用户id
     */
    void deleteByUId(String uid);
}
