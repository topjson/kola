package com.base.server.modules.sys.service;

import com.base.server.modules.user.entity.UserInfo;
import com.base.server.modules.user.entity.UserInfoVo;

import java.util.Map;

/**
 * 描述：系统service
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/12/16 15:27 星期一
 */
public interface SysService {
    Map<String, Object> login(UserInfo userInfo);

    UserInfoVo loginByName(String account);
}
