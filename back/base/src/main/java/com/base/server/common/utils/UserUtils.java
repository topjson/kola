package com.base.server.common.utils;

import com.base.server.modules.user.entity.UserInfoVo;
import org.apache.shiro.SecurityUtils;

import java.util.Objects;

/**
 * 描述：获取当前登录用户工具类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/17 13:58 星期五
 */
public class UserUtils {

    public static UserInfoVo getUserInfo() {
        return Objects.nonNull(SecurityUtils.getSubject())
                ? (UserInfoVo) SecurityUtils.getSubject().getPrincipal()
                : new UserInfoVo();
    }

    public static String getUid() {
        return getUserInfo().getId();
    }

}
