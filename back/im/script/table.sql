drop table if exists t_user_info;

/*==============================================================*/
/* Table: t_user_info                                           */
/*==============================================================*/
create table t_user_info
(
   id                   varchar(40) not null comment '主键id',
   name                 varchar(50) comment '用户名',
   header                 varchar(50) comment '头像',
   login_name           varchar(50) comment '登录名',
   sex                  varchar(10) comment '性别',
   add_time             datetime comment '添加时间',
   update_time          datetime comment '修改时间',
   add_user             varchar(40) comment '添加人',
   update_user          varchar(40) comment '修改人',
   status               int(2) comment '状态',
   primary key (id)
);

alter table t_user_info comment '用户表';
drop table if exists t_role_info;

/*==============================================================*/
/* Table: t_role_info                                           */
/*==============================================================*/
create table t_role_info
(
   id                   varchar(40) not null comment '主键id',
   name                 varchar(50) comment '角色名',
   description          varchar(100) comment '角色描述',
   add_time             datetime comment '添加时间',
   update_time          datetime comment '修改时间',
   add_user             varchar(40) comment '添加人',
   update_user          varchar(40) comment '修改人',
   status               int(2) comment '状态',
   primary key (id)
);

alter table t_role_info comment '角色表';
drop table if exists t_user_group_relation;

/*==============================================================*/
/* Table: t_user_group_relation                                 */
/*==============================================================*/
create table t_user_group_relation
(
   id                   varchar(40) not null comment '主键id',
   user_id              varchar(40) not null comment '用户id',
   user_group_id        varchar(40) not null comment '用户组id',
   primary key (id)
);

alter table t_user_group_relation comment '用户组与用户关联表';
drop table if exists t_user_role_relation;

/*==============================================================*/
/* Table: t_user_role_relation                                  */
/*==============================================================*/
create table t_user_role_relation
(
   id                   varchar(40) not null comment '主键id',
   user_id              varchar(40) not null comment '用户id',
   role_id              varchar(40) not null comment '角色id',
   primary key (id)
);

alter table t_user_role_relation comment '用户与角色关联表';
drop table if exists t_role_group_relation;

/*==============================================================*/
/* Table: t_role_group_relation                                 */
/*==============================================================*/
create table t_role_group_relation
(
   id                   varchar(40) not null comment '主键id',
   role_id              varchar(40) not null comment '角色id',
   user_group_id        varchar(40) not null comment '用户组id',
   primary key (id)
);

alter table t_role_group_relation comment '角色与用户组关联表';
drop table if exists t_user_group_info;

/*==============================================================*/
/* Table: t_user_group_info                                     */
/*==============================================================*/
create table t_user_group_info
(
   id                   varchar(40) not null comment '主键id',
   name                 varchar(40) not null comment '用户组名',
   description          varchar(40) comment '用户组描述',
   add_time             datetime comment '添加时间',
   update_time          datetime comment '修改时间',
   add_user             varchar(40) comment '添加人',
   update_user          varchar(40) comment '修改人',
   status               int(2) comment '状态',
   primary key (id)
);

alter table t_user_group_info comment '用户组表';
drop table if exists t_role_permission_relation;

/*==============================================================*/
/* Table: t_role_permission_relation                            */
/*==============================================================*/
create table t_role_permission_relation
(
   id                   varchar(40) not null comment '主键id',
   permission_id        varchar(40) not null comment '权限id',
   role_id              varchar(40) not null comment '角色id',
   primary key (id)
);

alter table t_role_permission_relation comment '角色与权限关联表';
drop table if exists t_permission_info;

/*==============================================================*/
/* Table: t_permission_info                                     */
/*==============================================================*/
create table t_permission_info
(
   id                   varchar(40) not null comment '主键id',
   p_type               varchar(30) not null comment '权限类型',
   description          varchar(100) comment '权限描述',
   add_time             datetime comment '添加时间',
   update_time          datetime comment '修改时间',
   add_user             varchar(40) comment '添加人',
   update_user          varchar(40) comment '修改人',
   status               int(2) comment '状态',
   primary key (id)
);

alter table t_permission_info comment '权限表';
drop table if exists t_menu_permission_relation;

/*==============================================================*/
/* Table: t_menu_permission_relation                            */
/*==============================================================*/
create table t_menu_permission_relation
(
   id                   varchar(40) not null comment '主键id',
   menu_id              varchar(40) not null comment '菜单id',
   permission_id        varchar(40) not null comment '权限id',
   primary key (id)
);

alter table t_menu_permission_relation comment '菜单权限关联表';
drop table if exists t_operation_permission_relation;

/*==============================================================*/
/* Table: t_operation_permission_relation                       */
/*==============================================================*/
create table t_operation_permission_relation
(
   id                   varchar(40) not null comment '主键id',
   operate_id           varchar(40) not null comment '功能操作id',
   permission_id        varchar(40) not null comment '权限id',
   primary key (id)
);

alter table t_operation_permission_relation comment '权限与功能操作关联表';
drop table if exists t_menu_info;

/*==============================================================*/
/* Table: t_menu_info                                           */
/*==============================================================*/
create table t_menu_info
(
   id                   varchar(40) not null comment '主键id',
   icon                 varchar(40) comment '菜单图标',
   name                 varchar(50) comment '菜单名称',
   url                  varchar(100) comment '菜单url',
   p_id                 varchar(40) comment '菜单父id',
   description          varchar(100) comment '菜单描述',
   add_time             datetime comment '添加时间',
   update_time          datetime comment '修改时间',
   add_user             varchar(40) comment '添加人',
   update_user          varchar(40) comment '修改人',
   status               int(2) comment '状态',
   order_index          int comment '排序',
   primary key (id)
);

alter table t_menu_info comment '菜单表';
drop table if exists t_operation_info;

/*==============================================================*/
/* Table: t_operation_info                                      */
/*==============================================================*/
create table t_operation_info
(
   id                   varchar(40) not null comment '主键id',
   name                 varchar(50) not null comment '操作名称',
   code                 varchar(50) not null comment '操作编码',
   prefix_url           varchar(100) comment '拦截url前缀',
   p_id                 varchar(40) comment '操作父id',
   add_time             datetime comment '添加时间',
   update_time          datetime comment '修改时间',
   add_user             varchar(40) comment '添加人',
   update_user          varchar(40) comment '修改人',
   status               int(2) comment '状态',
   primary key (id)
);

alter table t_operation_info comment '功能操作表';
