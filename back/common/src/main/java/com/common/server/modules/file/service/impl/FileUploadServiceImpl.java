package com.common.server.modules.file.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.common.server.modules.file.entity.FileEntity;
import com.common.server.modules.file.mapper.FileMapper;
import com.common.server.modules.file.service.FileUploadService;
import org.springframework.stereotype.Service;

/**
 * 描述：文件上传服务实现类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/10/8 14:37 星期二
 */
@Service
public class FileUploadServiceImpl extends ServiceImpl<FileMapper, FileEntity> implements FileUploadService {
}
