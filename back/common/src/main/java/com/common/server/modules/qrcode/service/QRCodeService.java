package com.common.server.modules.qrcode.service;

import com.common.server.modules.qrcode.entity.ZXingCode;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * 描述：二维码服务
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/10/12 15:34 星期六
 */
public interface QRCodeService {

    String decode(File qrFile);

    /**
     * 使用 ZXing 生成二维码(默认logo)
     *
     * @param zXingCode
     * @param request
     * @param response
     */
    void genZXingCode(ZXingCode zXingCode, HttpServletRequest request, HttpServletResponse response);

    /**
     * 生成自定义logo二维码
     * @param file
     * @param zXingCode
     * @param request
     * @param response
     */
    void genZXingSelfCode(MultipartFile file, ZXingCode zXingCode, HttpServletRequest request, HttpServletResponse response);
}
