package com.common.server.modules.log.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.server.common.response.ResponseVo;
import com.common.server.modules.log.entity.SysLog;
import com.common.server.modules.log.service.SysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 系统日志表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-11-22
 */
@Api(tags = "系统日志表")
@RestController
@RequestMapping("api/rest/sysLog")
public class SysLogController {
    private final Logger logger = LoggerFactory.getLogger(SysLogController.class);

    @Autowired
    public SysLogService iSysLogService;

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public ResponseVo getSysLogList(SysLog sysLog) {
        try {
            QueryWrapper<SysLog> queryWrapper = new QueryWrapper<>(sysLog);
            List<SysLog> result = iSysLogService.list(queryWrapper);
            return ResponseVo.ok(result);
        } catch (Exception e) {
            logger.error("sysLogList -=- {}", e.toString());
            return ResponseVo.error(e.getMessage());
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public ResponseVo getSysLogListPage(SysLog sysLog) {
        try {
            Page<SysLog> page = new Page<>();
            QueryWrapper<SysLog> queryWrapper = new QueryWrapper<>(sysLog);
            IPage<SysLog> result = iSysLogService.page(page, queryWrapper);
            return ResponseVo.ok(result);
        } catch (Exception e) {
            logger.error("getSysLogList -=- {}", e.toString());
            return ResponseVo.error(e.getMessage());
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public ResponseVo sysLogSave(SysLog sysLog) {
        int count = 0;
        try {
            count = iSysLogService.save(sysLog) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogSave -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public ResponseVo sysLogUpdate(SysLog sysLog) {
        int count = 0;
        try {
            count = iSysLogService.save(sysLog) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogUpdate -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public ResponseVo sysLogDelete(@PathVariable String id) {
        int count = 0;
        try {
            count = iSysLogService.removeById(id) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogDelete -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public ResponseVo deleteBatchIds(@RequestParam List<Long> ids) {
        int count = 0;
        try {
            count = iSysLogService.removeByIds(ids) ? 1 : 0;
        } catch (Exception e) {
            logger.error("sysLogBatchDelete -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }
}

