package com.common.server.modules.log.service;

import com.common.server.modules.log.entity.SysLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统日志表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-11-22
 */
public interface SysLogService extends IService<SysLog> {

}
