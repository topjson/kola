package com.common.server.modules.api;

import com.common.server.common.annotation.FeignClient;
import com.common.server.common.response.ResponseVo;
import com.common.server.modules.api.entity.MenuDto;
import com.common.server.modules.api.entity.UserDto;
import com.common.server.modules.api.entity.UserVo;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;

/**
 * 描述：后台权限管理服务 Api
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/1 9:43
 */
@FeignClient
public interface PermissionApi {

    /**
     * 登录服务
     *
     * @param userVo 用户信息
     * @return ResponseVo
     */
    @RequestLine("POST /login")
    ResponseVo login(@QueryMap UserVo userVo);

    /**
     * 验证token的有效性
     *
     * @param token 登录信息
     * @return
     */
    @RequestLine("POST /validToken/{token}")
    ResponseVo validToken(@Param("token") String token);

    /**
     * 登出服务
     *
     * @return
     */
    @RequestLine("GET /logout")
    ResponseVo logout();

    /**
     * 修改密码
     *
     * @param userDto
     * @return
     */
    @RequestLine("POST /user/updPwd")
    ResponseVo updPwd(@QueryMap UserDto userDto);

    /**
     * 根据条件查询菜单权限
     *
     * @return
     */
    @RequestLine("GET /menu/getMenuTree")
    ResponseVo getMenuTree(@QueryMap MenuDto menuDto);

}
