package com.common.server.modules.file.service;

import com.common.server.common.response.ResponseVo;
import com.common.server.modules.file.entity.FileEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 文件信息表 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-10-08
 */
public interface FileService extends IService<FileEntity> {

    ResponseVo save(MultipartFile file, String path, FileEntity entity);

    /**
     * word to html
     *
     * @param file
     * @param ip
     * @return
     */
    String word2html(MultipartFile file, String ip);
}
