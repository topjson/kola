package com.common.server.modules.log.mapper;

import com.common.server.modules.log.entity.SysLogErr;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统异常日志 Mapper 接口
 * </p>
 *
 * @author ostrich
 * @since 2019-10-09
 */
public interface SysLogErrMapper extends BaseMapper<SysLogErr> {

}
