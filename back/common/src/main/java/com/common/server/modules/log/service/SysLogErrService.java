package com.common.server.modules.log.service;

import com.common.server.modules.log.entity.SysLogErr;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统异常日志 服务类
 * </p>
 *
 * @author ostrich
 * @since 2019-10-09
 */
public interface SysLogErrService extends IService<SysLogErr> {

}
