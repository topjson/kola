package com.common.server.common.config.zxingcode;

import lombok.Data;

import java.awt.*;

/**
 * 描述：LogoConfig 配置类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2019/10/12 17:51 星期六
 */
@Data
public class LogoConfig {
    // logo默认边框颜色
    public static final Color DEFAULT_BORDERCOLOR = Color.WHITE;
    // logo默认边框宽度
    public static final int DEFAULT_BORDER = 2;
    // logo大小默认为照片的1/5
    public static final int DEFAULT_LOGOPART = 5;
    // 默认边框宽度
    private final int border = DEFAULT_BORDER;
    // 边框颜色
    private final Color borderColor;
    // 边框外围宽度
    private final int logoPart;

    /**
     * 二维码无参构造函数 默认设置Logo图片底色白色宽度2
     */
    public LogoConfig() {
        this(DEFAULT_BORDERCOLOR, DEFAULT_LOGOPART);
    }

    /**
     * 二维码有参构造函数
     *
     * @param borderColor 边框颜色
     * @param logoPart    边框宽度
     */
    public LogoConfig(Color borderColor, int logoPart) {
        // 设置边框
        this.borderColor = borderColor;
        // 设置边框宽度
        this.logoPart = logoPart;
    }
}
