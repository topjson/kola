package com.common.server.modules.dict.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.common.server.common.response.ResponseVo;
import com.common.server.common.util.StringUtils;
import com.common.server.common.util.Tree;
import com.common.server.modules.dict.entity.Dict;
import com.common.server.modules.dict.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 字典表 前端控制器
 * </p>
 * <p>
 * 作者: ostrich
 * 时间: 2019-10-09
 */
@Api(tags = "字典")
@RestController
@RequestMapping("api/rest/dict")
public class DictController {
    private final Logger logger = LoggerFactory.getLogger(DictController.class);

    @Autowired
    public DictService iDictService;

    @ApiOperation("根据id查询")
    @GetMapping(value = "/{id}")
    public ResponseVo getTreeList(@PathVariable String id) {
        Dict dict = iDictService.getById(id);
        return ResponseVo.ok(dict);
    }

    @ApiOperation("获取字典树")
    @GetMapping(value = "/tree")
    public ResponseVo getTreeList(Dict dict) {
        List<Tree<Dict>> dictTree = iDictService.getDictTree(dict);
        return ResponseVo.ok(dictTree);
    }

    @ApiOperation("根据字典分类查询code和value")
    @GetMapping("/kv/{type}")
    public ResponseVo getKVList(@PathVariable String type) {
        return ResponseVo.ok(iDictService.getKVList(type));
    }

    @ApiOperation("无分页查询数据")
    @GetMapping(value = "/list")
    public ResponseVo getDictList(Dict dict) {
        try {
            QueryWrapper<Dict> queryWrapper = new QueryWrapper<>(dict);
            List<Dict> result = iDictService.list(queryWrapper);
            return ResponseVo.ok(result);
        } catch (Exception e) {
            logger.error("dictList -=- {}", e.toString());
            return ResponseVo.error(e.getMessage());
        }
    }

    @ApiOperation("分页查询数据")
    @GetMapping("/list/page")
    public ResponseVo getDictListPage(Dict dict) {
        try {
            Page<Dict> page = new Page<>();
            QueryWrapper<Dict> queryWrapper = new QueryWrapper<>(dict);
            IPage<Dict> result = iDictService.page(page, queryWrapper);
            return ResponseVo.ok(result);
        } catch (Exception e) {
            logger.error("getDictList -=- {}", e.toString());
            return ResponseVo.error(e.getMessage());
        }
    }

    @ApiOperation("新增数据")
    @PostMapping(value = "/save")
    public ResponseVo save(Dict dict) {
        try {
            dict.setId(StringUtils.uuidGenerator());
            dict.setAddDate(LocalDateTime.now());
            dict.setIsDelete(0);
            iDictService.saveOrUpdate(dict);
            return ResponseVo.ok(dict);
        } catch (Exception e) {
            logger.error("dictSave -=- {}", e.toString());
            return ResponseVo.error();
        }
    }

    @ApiOperation("修改数据")
    @PutMapping(value = "/upd")
    public ResponseVo saveOrUpdate(Dict dict) {
        try {
            dict.setUpdateDate(LocalDateTime.now());
            iDictService.saveOrUpdate(dict);
            return ResponseVo.ok(dict);
        } catch (Exception e) {
            logger.error("dictUpdate -=- {}", e.toString());
            return ResponseVo.error();
        }
    }

    @ApiOperation("根据id删除对象")
    @DeleteMapping(value = "del/{id}")
    public ResponseVo dictDelete(@PathVariable String id) {
        int count = 0;
        try {
            count = iDictService.removeById(id) ? 1 : 0;
        } catch (Exception e) {
            logger.error("dictDelete -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }

    @ApiOperation("批量删除对象")
    @DeleteMapping(value = "del/batch")
    public ResponseVo deleteBatchIds(@RequestParam List<Long> ids) {
        int count = 0;
        try {
            count = iDictService.removeByIds(ids) ? 1 : 0;
        } catch (Exception e) {
            logger.error("dictBatchDelete -=- {}", e.toString());
        }
        return ResponseVo.ok(count);
    }
}

