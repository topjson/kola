/*
Navicat MySQL Data Transfer

Source Server         : dev
Source Server Version : 50716
Source Host           : localhost:3306
Source Database       : common_dev

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2019-12-31 16:15:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_common_message
-- ----------------------------
DROP TABLE IF EXISTS `t_common_message`;
CREATE TABLE `t_common_message` (
  `id` varchar(40) NOT NULL COMMENT '主键',
  `business_id` varchar(40) DEFAULT NULL COMMENT '通知信息业务id',
  `types` varchar(30) DEFAULT NULL COMMENT '类型（通知、公共）',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `shortly` varchar(200) DEFAULT NULL COMMENT '简要',
  `content` longtext COMMENT '正文',
  `recipient` varchar(500) DEFAULT NULL COMMENT '收件人id',
  `attachment` varchar(500) DEFAULT NULL COMMENT '附件id',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `add_user` varchar(40) DEFAULT NULL COMMENT '添加人',
  `status` int(2) DEFAULT NULL COMMENT '状态（0未读，1已读，2忽略）',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息表';

-- ----------------------------
-- Table structure for t_common_push
-- ----------------------------
DROP TABLE IF EXISTS `t_common_push`;
CREATE TABLE `t_common_push` (
  `id` varchar(40) NOT NULL COMMENT '主键id',
  `business_id` varchar(30) DEFAULT NULL COMMENT '业务id',
  `accept_client_id` varchar(40) DEFAULT NULL COMMENT '接收客户端id',
  `start_client_id` varchar(40) DEFAULT NULL COMMENT '发起客户端id',
  `push_msg` varchar(500) DEFAULT NULL COMMENT '推送的消息',
  `push_date` datetime DEFAULT NULL COMMENT '推送时间',
  `push_result` datetime DEFAULT NULL COMMENT '处理时间',
  `types` varchar(30) DEFAULT NULL COMMENT '类型（消息（message）、告警(warning)）',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `start_client_id` (`start_client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推送表';

-- ----------------------------
-- Table structure for t_common_push_template
-- ----------------------------
DROP TABLE IF EXISTS `t_common_push_template`;
CREATE TABLE `t_common_push_template` (
  `id` varchar(40) NOT NULL COMMENT '主键id',
  `content` varchar(500) DEFAULT NULL COMMENT '模板内容',
  `types` varchar(50) DEFAULT NULL COMMENT '模板类型',
  `status` int(2) DEFAULT NULL COMMENT '模板状态',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(225) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `types` (`types`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='推送模板表';

-- ----------------------------
-- Table structure for t_common_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `t_common_sys_log`;
CREATE TABLE `t_common_sys_log` (
  `id` varchar(40) DEFAULT NULL COMMENT '主键',
  `sys_type` varchar(20) DEFAULT NULL COMMENT '系统类型',
  `op_type` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `op_content` varchar(200) DEFAULT NULL COMMENT '操作内容',
  `op_user` varchar(40) DEFAULT NULL COMMENT '操作人',
  `op_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  `op_ip` varchar(40) DEFAULT NULL COMMENT '操作ip',
  UNIQUE KEY `id` (`id`) USING BTREE,
  KEY `op_user` (`op_user`),
  KEY `op_type` (`op_type`) USING BTREE,
  KEY `op_time` (`op_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统日志表';

-- ----------------------------
-- Table structure for t_common_warning
-- ----------------------------
DROP TABLE IF EXISTS `t_common_warning`;
CREATE TABLE `t_common_warning` (
  `id` varchar(40) NOT NULL COMMENT '主键',
  `level` varchar(30) DEFAULT NULL COMMENT '级别',
  `types` varchar(40) DEFAULT NULL COMMENT '类型',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `content` text COMMENT '内容',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `add_user` varchar(40) DEFAULT NULL COMMENT '添加人',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警通知表';

-- ----------------------------
-- Table structure for t_common_warning_config
-- ----------------------------
DROP TABLE IF EXISTS `t_common_warning_config`;
CREATE TABLE `t_common_warning_config` (
  `id` varchar(40) NOT NULL COMMENT '主键',
  `warning_id` varchar(40) DEFAULT NULL COMMENT '告警配置表主键',
  `norm_obj` varchar(40) DEFAULT NULL COMMENT '指标对象',
  `formula` varchar(40) DEFAULT NULL COMMENT '公式',
  `threshold` varchar(40) DEFAULT NULL COMMENT '阈值',
  `logic` varchar(20) DEFAULT NULL COMMENT '关系',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='告警指标配置表';

-- ----------------------------
-- Table structure for t_dict
-- ----------------------------
DROP TABLE IF EXISTS `t_dict`;
CREATE TABLE `t_dict` (
  `id` varchar(36) NOT NULL COMMENT 'id',
  `p_id` varchar(36) DEFAULT NULL COMMENT '父级id',
  `code` varchar(50) DEFAULT NULL COMMENT '字典编码',
  `value` varchar(50) DEFAULT NULL COMMENT '字典值',
  `type` varchar(30) DEFAULT NULL COMMENT '字典分类',
  `back_img` varchar(100) DEFAULT NULL COMMENT '背景图片',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `is_delete` int(1) DEFAULT NULL COMMENT '是否删除',
  `add_date` datetime DEFAULT NULL COMMENT '添加时间',
  `remark` varchar(100) DEFAULT NULL COMMENT '添加人',
  `value_en` varchar(100) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL COMMENT '修改时间',
  `order_index` varchar(10) DEFAULT '0' COMMENT '序号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Table structure for t_file
-- ----------------------------
DROP TABLE IF EXISTS `t_file`;
CREATE TABLE `t_file` (
  `id` varchar(40) DEFAULT NULL COMMENT '主键id',
  `code` varchar(40) DEFAULT NULL COMMENT '文件编码',
  `ip` varchar(100) DEFAULT NULL COMMENT '服务器ip(用于下载)',
  `url` varchar(100) DEFAULT NULL COMMENT '文件url',
  `old_name` varchar(100) DEFAULT NULL COMMENT '文件原名',
  `name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `type` varchar(30) DEFAULT NULL COMMENT '文件类型',
  `size` varchar(10) DEFAULT NULL COMMENT '文件大小',
  `order_index` varchar(10) DEFAULT NULL COMMENT '文件排序',
  `add_user` varchar(40) DEFAULT NULL COMMENT '添加人',
  `add_date` datetime DEFAULT NULL COMMENT '添加时间',
  `status` varchar(10) DEFAULT NULL COMMENT '状态（1_P：PC端首页轮播图；1_M：移动端首页轮播图；2_P：PC端业务领域；2_M:移动端业务领域; 3：用于新闻视频；4：简历附件；5：新闻附件;6：Word2Html 的图片;7_P：PC端关于我们；7_M:移动端关于我们；8：微信公众号图片）',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件信息表';

-- ----------------------------
-- Table structure for t_sys_log_err
-- ----------------------------
DROP TABLE IF EXISTS `t_sys_log_err`;
CREATE TABLE `t_sys_log_err` (
  `id` varchar(50) NOT NULL COMMENT '主键',
  `sys_type` varchar(50) DEFAULT NULL COMMENT '系统类型',
  `account` varchar(20) DEFAULT NULL COMMENT '帐号',
  `ip_source` varchar(20) DEFAULT NULL COMMENT 'IP来源',
  `ip_address` varchar(255) DEFAULT NULL COMMENT 'IP地址',
  `status` varchar(64) DEFAULT NULL COMMENT '状态：unchecked，checked，fixed',
  `url` varchar(1500) DEFAULT NULL COMMENT '错误URL',
  `content` text COMMENT '出错信息',
  `request_param` text COMMENT '请求参数',
  `create_time` datetime DEFAULT NULL COMMENT '出错时间',
  `stack_trace` longtext COMMENT '出错异常堆栈',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统异常日志';
