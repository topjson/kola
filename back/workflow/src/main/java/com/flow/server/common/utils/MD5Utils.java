package com.flow.server.common.utils;


/**
 * @author hutongfu
 * @description: md5 加密工具类
 * @since 2019/6/6 11:08
 */
public class MD5Utils {
    public static final String SALT = "common";

    private static final String ALGORITHM_NAME = "md5";

    private static final int HASH_ITERATIONS = 2;

//    public static String encrypt(String pswd) {
//        return new SimpleHash(ALGORITHM_NAME, pswd, ByteSource.Util.bytes(SALT), HASH_ITERATIONS).toHex();
//    }
//
//    public static String encrypt(String username, String pswd) {
//        return new SimpleHash(ALGORITHM_NAME, pswd, ByteSource.Util.bytes(username + SALT), HASH_ITERATIONS).toHex();
//    }
}
