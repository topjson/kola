package com.flow.server.modules.base.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flow.server.modules.base.service.IBaseService;
import org.activiti.engine.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 描述：基础service实现类
 * <p>
 * 作者：hutongfu
 * 时间：2020/1/18 11:27 星期六
 */
public class IBaseServiceImpl<T> implements IBaseService<T> {

    public final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    public RepositoryService repositoryService;
    @Resource
    public HistoryService historyService;
    @Resource
    public RuntimeService runtimeService;
    @Resource
    public TaskService taskService;
    @Resource
    public ManagementService managementService;
    @Resource
    public FormService formService;
    @Resource
    public IdentityService identityService;
    @Resource
    public DynamicBpmnService dynamicBpmnService;
    @Resource
    public ObjectMapper objectMapper;

    @Override
    public void deleteById(String modelId) {

    }

    @Override
    public void deleteByObject(T t) {

    }
}
