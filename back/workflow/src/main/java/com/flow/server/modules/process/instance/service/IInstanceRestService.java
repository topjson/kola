package com.flow.server.modules.process.instance.service;

import com.flow.server.modules.base.service.IBaseService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 描述：流程实例service
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/19 9:13 星期日
 */
public interface IInstanceRestService extends IBaseService {

    /**
     * 发起流程实例
     *
     * @param request
     * @return
     */
    boolean startProcessInstance(HttpServletRequest request);

    /**
     * 分页查询流程实例
     *
     * @param request
     * @return
     */
    Map<String, Object> getListPage(HttpServletRequest request);

    /**
     * 根据流程实例id获取运行时流程图
     *
     * @param instanceId
     * @param response
     */
    void getRunWorkflowImage(String instanceId, HttpServletResponse response);
}
