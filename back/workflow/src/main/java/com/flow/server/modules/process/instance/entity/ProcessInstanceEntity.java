package com.flow.server.modules.process.instance.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述：流程实例 实体类
 * <p>
 * 作者：Ostrich Hu
 * 时间：2020/1/19 12:09 星期日
 */
@Data
public class ProcessInstanceEntity implements Serializable {

    private static final long serialVersionUID = -6009766590145859674L;

    //流程名称
    private String name;
    //流程实例id
    private String instanceId;
    //流程key
    private String processKey;
    //流程定义id
    private String processDefId;
    //流程部署id
    private String deploymentId;
    //流程业务id
    private String businessId;
    //流程发起人id
    private String startUserId;
    //流程发起人
    private String startUserName;
    //当前节点名称
    private String currNodeName;
    //当前节点定义key
    private String currNodeDefKey;
    //当前节点id
    private String currNodeId;
    //上一节点id
    private String preNodeId;
    //过期日期
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date dueDate;
    //最近办理时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date recentlyDoTime;
    //锁定时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date claimTime;
    //锁定人
    private String claimUserName;
    //流程状态
    private String status;
    //作废时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date deletedTime;
    //添加时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;
    //是否已读
    private String isView;
    //流程描述
    private String description;
    //是否结束
    private boolean ended;
    //是否挂起
    private boolean suspended;

    public ProcessInstanceEntity(ProcessInstance processInstance) {
        this.name = processInstance.getName();
        this.instanceId = processInstance.getProcessInstanceId();
        this.processDefId = processInstance.getProcessDefinitionId();
        this.deploymentId = processInstance.getDeploymentId();
        this.processKey = processInstance.getProcessDefinitionKey();
        this.businessId = processInstance.getBusinessKey();
        this.startUserId = processInstance.getStartUserId();
        this.startDate = processInstance.getStartTime();
        this.description = processInstance.getDescription();
        this.currNodeDefKey = processInstance.getActivityId();
        this.ended = processInstance.isEnded();
        this.suspended = processInstance.isSuspended();
    }
}
