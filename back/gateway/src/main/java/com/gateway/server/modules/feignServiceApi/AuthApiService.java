package com.gateway.server.modules.feignServiceApi;

import com.gateway.server.common.response.CommonResponse;
import com.gateway.server.modules.feignServiceApi.entity.LoginVo;
import feign.QueryMap;
import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 描述: AuthApiService 权限认证服务
 * <p>
 * 作者: hu_to
 * 时间: 2020/02/27 15:51
 */
@FeignClient(value = "base")
public interface AuthApiService {

    /**
     * 登录
     *
     * @param vo
     * @return
     */
    @PostMapping(value = "/api/rest/sys/login")
    Response login(LoginVo vo);

    /**
     * 登出
     *
     * @param account
     * @return
     */
    @GetMapping(value = "/api/rest/sys/logout/{account}")
    CommonResponse login(@PathVariable("account") String account);

    /**
     * 验证token
     *
     * @param token
     * @return
     */
    @PostMapping(value = "/api/rest/sys/validToken/{token}")
    CommonResponse validToken(@PathVariable("token") String token);
}
