package com.gateway.server.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 描述：自定义异常
 * <p>
 * 作者：HuTongFu
 * 时间：2019/7/12 15:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerException extends RuntimeException {

    private static final long serialVersionUID = 244180471316154668L;

    private String msg;
    private int code = 500;

    public CustomerException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public CustomerException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public CustomerException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public CustomerException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

}
