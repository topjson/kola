package com.gateway.server.common.config;

import com.gateway.server.common.filter.AuthFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述: GatewayConfig 网关配置类
 * <p>
 * 作者: hu_to
 * 时间: 2020/02/27 16:10
 */
@Configuration
public class GatewayConfig {

    @Bean
    public AuthFilter tokenFilter() {
        return new AuthFilter();
    }
}
