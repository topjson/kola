import Vue from 'vue'
// import ElementUI from 'element-ui'

const _self = new Vue();

/**
 * 警告提示（1.5秒后自动关闭）
 * @param message 提示内容
 */
export const warningMsg = (message) => {
    _self.$message({showClose: false, message: message, type: 'warning', duration: 1500});
};

/**
 * 成功提示（1.5秒后自动关闭）
 * @param message 提示内容
 */
export const successMsg = (message) => {
    _self.$message({showClose: false, message: message, type: 'success', duration: 1500});
};

/**
 * 错误提示（1.5秒后自动关闭）
 * @param message 提示内容
 */
export const errorMsg = (message) => {
    _self.$message({showClose: false, message: message, type: 'error', duration: 1500});
};

/**
 * @param {Array} arr
 * @returns {Array}
 */
export const getBreadCrumbList = (arr) => {
    let res = arr.map(item => {
        return {
            icon: (item.meta && item.meta.icon) || '',
            name: item,
        }
    });
    return [{name: '主页', icon: 'ios-home-outline'}, ...res]
};


/**
 * 时间格式化
 * @param date
 * @param fmt
 * @returns {*}
 */
export function formatDate(date, fmt) {
    let o = {
        'M+': date.getMonth() + 1, // 月份
        'd+': date.getDate(), // 日
        'h+': date.getHours(), // 小时
        'm+': date.getMinutes(), // 分
        's+': date.getSeconds(), // 秒
        'S': date.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
    }
    for (let k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)))
        }
    }
    return fmt
}


/**
 * 判断是否为null
 * @param str
 * @returns {string}
 */
export function judgeNull(str) {
    if (str === undefined || str === 'null' || str === null)
        return '';
}


/**
 * 将object 属性中 null值 转换为 ''
 * @param obj
 * @returns {string}
 */
export function objectExcludeNull(obj) {
    if (obj) {
        let resObj = {};
        for (let k of Object.keys(obj)) {
            resObj[k] = obj[k] === undefined || obj[k] === null || obj[k] === 'null' ? '' : obj[k];
        }
        return resObj;
    } else {
        return '';
    }
}
