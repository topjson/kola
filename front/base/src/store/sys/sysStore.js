/*
 *系统相关仓库（与业务逻辑无关）
 */
export default {
    state: {
        //标签数组
        tagsBarData: [],
        //菜单数组
        menuItemData: []
    },
    mutations: {
        //添加标签
        pushTag(state, payload) {
            if (payload) {
                let tempArr = state.tagsBarData.filter(obj => {
                    return obj.path === payload.path;
                });
                //第一次push
                if (tempArr.length === 0) {
                    state.tagsBarData.push(payload);
                }
            }
            //设置当前激活的tag颜色
            state.tagsBarData.forEach(obj => {
                if (payload && obj.path === payload.path) {
                    obj.type = 'primary';
                } else {
                    obj.type = 'info';
                }
            });
        },
        //移除标签
        removeTag(state, payload) {
            if (payload) {
                state.tagsBarData.splice(state.tagsBarData.indexOf(payload), 1);
            } else {
                state.tagsBarData = [];
            }
        },
        //添加菜单
        pullMenuItem(state, payload) {
            state.tagsBarData = payload;
        },
        //初始化菜单
        initMenus(state, payload) {
            state.menuItemData = payload;
        }
    },
    actions: {}
}
